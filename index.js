// load express module into application
const express = require('express'); 

// localhost port number
const port = 4000; 

// store express module to app variable
const app = express();

// //streaming of data and automatically parse the incoming json from our request.
app.use(express.json());


//Mock data
let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];

// access home page
app.get("/home", (request, response) => response.send('Welcome to the homepage'));

// access mock-up items
app.get("/items", (resquest, response) => response.send(items));

// access deleted items in mock-up
app.delete("/delete-item", (request, response) => {
    const deletedItem = items.pop();
    response.send(deletedItem)
})


// listen to the port
app.listen(port, ()=> console.log(`Server is running at port ${port}`))
